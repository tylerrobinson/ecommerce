const LocalStrategy = require('passport-local').Strategy
const User = require('../app/models/user')

module.exports = function (passport) {
	//session setup for persistent login sessions
	//passport needs to be able to serialize and deserialize users 

	//serialize
	passport.serializeUser(function (user, done) {
		done(null, user.id)
	})

	//deserialize
	passport.deserializeUser(function (id, done) {
		User.findById(id, function (err, user) {
			done(err, user)
		})
	})

/* By default, the local-register strategy looks for fields
 * in the POST body named 'username' and 'password'.
 */
	passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, email, password, done) {
		//asynchronous
		//User.findOne won't fire unless data is sent back
		process.nextTick(function () {
			//find a user whose email is the same as the form's email
			//we are checking to see if the user trying to login already exists
			email = email.toLocaleLowerCase()
			User.findOne({'local.email': email}, function (err, user) {
				//if any errors ,return the error
				if (err)
					return done(err)
				//check if user already exists
				if (user) {
					return done(null, false)				
				} else {
					//create user
					var newUser = new User()
					//set credentials and name
					//newUser.local.firstName = req.body.firstName
					//newUser.local.lastName = req.body.lastName
					newUser.local.email = email
					newUser.local.password = newUser.generateHash(password)
					//save user
					newUser.save(function (err) {
						if (err)
							throw err
						return done(null, newUser)
					})
				}
			})
		})
	}))

/* By default, the local-login strategy looks for fields
 * in the POST body named 'username' and 'password'.
 */
	passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, email, password, done) {
		email = email.toLocaleLowerCase()
		User.findOne({'local.email': email}, function (err, user) {
			if (err)
				return done(err)
			if (!user) //no user found
				return done(null, false, req.flash('error', 'User not found.'))
			if (!user.validPassword(password)) //wrong password
				return done(null, false, req.flash('error', 'Invalid password.'))
			return done(null, user)
		})
	}))
}