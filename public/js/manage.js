console.log('hello client')

function getQueryVariable (variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
          var pair = vars[i].split("=");
          if(pair[0] == variable){return pair[1];}
  }
  return(false);
}

var subscriptionid = getQueryVariable('subscriptionid')
var customerid = getQueryVariable('custid')

var vm = new Vue({
  el: '#app',
  data: {
    subscriptionid: subscriptionid,
    customerid: customerid
  },
  methods: {
    updatePlan: function () {
      console.log('clicked updatePlan')
      axios.put('/api/switch-plan', {
        subscriptionId: this.subscriptionid,
        customerId: this.customerid
      })
      .then(result => console.log(result))
      .catch(err => console.log(err.stack))
    }
  }
})