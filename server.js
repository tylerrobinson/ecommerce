//Set environment 
if (process.env.NODE_ENV !== 'production') {
	require('dotenv').config() //allows us to use .env variables in local mode
}

//basic express functionality
const express = require('express')
const app = express()
const port = process.env.PORT
const morgan = require('morgan')
const ejs = require('ejs')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

//payments setup
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY)

//database setup
const cache = require('memory-cache')
const mongoose = require('mongoose')
mongoose.Promise = Promise
const mongoUri = process.env.MONGODB_URI
mongoose.connect(mongoUri)

//auth and sessions setup
const flash = require('connect-flash')
const passport = require('passport')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)

//configure passport
require('./config/passport')(passport)

//configure auth and sessions
//reuse existing mongoose connection
app.use(session({
	secret: process.env.SECRET_SESSION_KEY,
	store: new MongoStore({ mongooseConnection: mongoose.connection })
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

console.log('booting in mode: ' + process.env.NODE_ENV)

//Configure express app
app.use(express.static('public')) // serve static resources. putting this up here so sessions will not be created for static resources
app.use(morgan('dev')) // log every request to console
app.use(cookieParser()) // read cookies, needed for authentication
app.use(bodyParser()) // get info from html forms
app.set('view-engine', 'ejs') // use ejs for templates
ejs.delimiter = '$' // custom delimiter for ejs templates

//Model for use in routes
const User = require('./app/models/user')

// Routes
app.get('/', (req, res) => {
	return res.render('index.ejs')
})

app.get('/signup', (req, res) => {
	res.render('signup.ejs', {
		planId: req.query.plan,
		errorMessage: req.flash('error'),
		successMessage: req.flash('success')
	})
})

app.get('/login', (req, res) => {
	res.render('login.ejs', {
		errorMessage: req.flash('error'),
		successMessage: req.flash('success')
	})
})

app.post('/signup', (req, res, next) => {
	passport.authenticate('local-signup', function (err, user, info) {
		if (err) {
			return next(err)
		}
		if (!user) {
			req.flash('error', 'That email address already has an account.')
			return res.redirect('/signup?plan=' + req.query.plan)
		}
		req.logIn(user, function (err) {
			if (err) {
				return next(err)
			} else {
				//req.flash('success', 'You created an account!')
				return res.redirect('/checkout?plan=' + req.query.plan)
			}
		})
	})(req, res, next)
})

app.post('/login', function (req, res, next) {
	if (req.body.email === '' || req.body.password === '') {
		req.flash('errorMessage', 'Email or password cannot be blank.')
		return res.redirect('/login')
	} 
	next()
}, function (req, res, next) {
	passport.authenticate('local-login', function(err, user, info) {
		if (err) {
			return next(err)
		}
		if (!user) {
			return res.redirect('/login')
		}
			req.logIn(user, function(err) {
				if (err) {
					return next(err);
				}
				return res.redirect('/manage')	
			})
	})(req, res, next)
})

app.get('/checkout', (req, res) => {
	var planDescription
	var planPrice
	switch (req.query.plan) {
		case 'plan_DXvEiCY575aBPD':
			planDescription = 'DynaMed Classic Subscription'
			planPrice = 299
			break
		case 'plan_DXvDYbrfv6weRz':
			planDescription = 'DynaMed Plus Subscription'
			planPrice = 399
			break
		case 'plan_DXvD9g8PwyxTMk':
			planDescription = 'DynaMed Premier Subscription'
			planPrice = 499
			break
		default:
			planDescription = 'Error'
	}

	return res.render('checkout.ejs', {
		email: req.user.local.email,
		plan: req.query.plan,
		planDescription: planDescription,
		planPrice: planPrice
	})
})

app.post('/checkout', async (req, res) => {
	try {
		let customer = await stripe.customers.create({
			email: req.body.email,
			source: req.body.stripeToken
		})
		let user = await User.findById(req.user.id)
		user.set({
			'stripe.customerId': customer.id
		})
		await user.save()
		return res.redirect('/review?plan=' + req.body.planId)
	} catch (err) {
		return res.status(500).send(err.stack)
	}
})

app.get('/review', async (req, res) => {

	/**
	 * Tax Quote call goes here...
	 */

	let taxPercent = 6

	return res.render('review.ejs', {
		plan: req.query.plan,
		taxPercent
	})
})

app.post('/subscribe', async (req, res) => {
	try {
		let subscription = await stripe.subscriptions.create({
			customer: req.user.stripe.customerId,
			items: [{plan: req.body.planId}],
			tax_percent: 6
		})

		/**
		 * Tax Invoice call goes here...
		 */

		/**
		 * Fulfillment call goes here...
		 */
		
		 let user = await User.findById(req.user.id)
		user.set({
			'stripe.subscriptionId': subscription.id
		})
		await user.save()
		return res.redirect('/manage')
	} catch (err) {
		return res.status(500).send(err.stack)
	}
})

app.get('/manage', async (req, res) => {
	try {
		let subscriptions = await stripe.subscriptions.list({ customer: req.user.stripe.customerId })
		
		let invoices = await stripe.invoices.list({
			limit: 1,
			customer: req.user.stripe.customerId
		})
		
		return res.render('manage.ejs', {
			email: req.user.local.email,
			nickname: subscriptions.data[0].plan.nickname,
			subscriptionId: subscriptions.data[0].id,
			invoiceText: 'You paid $' + invoices.data[0].amount_paid/100 + ' on ' + Date(invoices.data[0].date)
		})		
	} catch (err) {
		return res.status(500).send(err.stack)
	}
})

app.get('/logout', (req, res) => {
	req.logout()
	res.redirect('/')
})

/*
app.put('/api/switch-plan', async (req, res) => {
	try {
		let subscription = await stripe.subscriptions.retrieve(req.body.subscriptionId)
		let updatedSubscription = await stripe.subscriptions.update(req.body.subscriptionId, {
			cancel_at_period_end: false,
			items: [{
				id: subscription.items.data[0].id,
				plan: 'plan_DXvD9g8PwyxTMk' //hardcoded the premier plan for now
			}]
		})
		let invoice = await stripe.invoices.create({
			customer: req.body.customerId
		})
		await stripe.invoices.pay(invoice.id)
		return res.status(200).send(updatedSubscription)
	} catch (err) {
		return res.status(500).send(err.stack)
	}
})
*/

//Error handler for all routes
app.use(function (err, req, res, next) {
	console.log('server.js error handler:')
	console.log(JSON.stringify(err.message))
	console.log(err.stack)
	return res.status(500).send(err.message + ' ' + err.stack)
})

//Anything else gets a 404
app.get('*', function (req, res) {
	return res.status(404).send('404 Not Found')
})

//Launch app
app.listen(port, () => { console.log('App is listening on ' + port ) })